<?php
use App\Models\BetaSystem\Coupon as THIS;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::any('_job/{action?}', '_Dev\CronJob@index');
Route::get('/', function () {
    return view('welcome');
});
