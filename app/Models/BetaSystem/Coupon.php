<?php

namespace App\Models\BetaSystem;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $table='coupons';
    public $timestamps = false;

    public static function getListCoupon($pageSize = 12){
        return self::select('code','value','created','expired','status')
        // ->orderBy('created_at', 'DESC')
        ->paginate($pageSize);
    }
    public function getAllWithPage($codition = [], $pageSize = 12){
        $model = $this;
        // lay ban ghi co dieu kien
        if(!empty($codition)){
            foreach($codition as $key => $value){
                $model = $model->where($key, $value);
            }
        }
        return $model->orderBy('created_at', 'DESC')
                    ->paginate($pageSize);
    }
    public static function fakeCoupon($length = 10){
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        $objToSave = [
            [
                'code' => $randomString,
                'value' => \rand(10,100),
                'created' => '1587108576',
                'expired' => '1587108576',
                'status'  => \rand(0,1),
            ],
            [
                'code' => $randomString,
                'value' => \rand(10,100),
                'created' => '1587108576',
                'expired' => '1587108576',
                'status'  => \rand(0,1),
            ]
        ];
        self::insertGetId($objToSave[array_rand($objToSave)]);
    }
}
