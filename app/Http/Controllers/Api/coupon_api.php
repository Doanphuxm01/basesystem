<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;
use App\Models\BetaSystem\Coupon as THIS;
use Validator;
use App\Http\Requests\Coupon as CouponRequest;
class coupon_api extends AppApi
{
    public function index($action = '')
    {
        $action = str_replace('-', '_', $action);
        if (method_exists($this, $action)) {
            return $this->$action();
        } else {
            return $this->list();
        }
    }

    public function list(){
        $lsCp = THIS::getListCoupon();
        if(isset($lsCp)){
            foreach($lsCp as $key => $value){
                if((!isset($value->created )) && (!isset($value->expired )) ){
                    return $this->outPutError('Không tìm thấy dữ liệu!',Response::HTTP_INTERNAL_SERVER_ERROR);
                }else{
                    $lsCp[$key]->created = date("Y-m-d H:i:s",$value->created);
                    $lsCp[$key]->expired = date("Y-m-d H:i:s",$value->expired);
                }
            }
        }
        if(!empty($lsCp)){
            return $this->outputDone($lsCp, "Lấy dữ liệu thành công",Response::HTTP_OK);
        }
        return $this->outPutError('Không tìm thấy dữ liệu!',Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function input(){
        $request = request();
        $validator = Validator::make($request->all(), [
            'code'       => 'required|unique:coupons,code|max:10|min:3',
            'value'      => 'required|max:100',
            'status'      => 'required',
            'created'    =>  'required|date',
            'expired'    =>  'required|date|after_or_equal:created'
        ]);
        if($validator->fails()) {
            return $this->outPutError($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        /**
         * code 
         * value
         * created
         * expired
         * status
         */
        $data = array(
            'code'=>$request->code,
            'status'=>$request->status,
            'created'=>strtotime(str_replace('/', '-', $request->created)),
            'value'=>$request->value,
            'expired'=>strtotime(str_replace('/', '-', $request->expired)),
        );
        if(THIS::insert($data)){
            return $this->outputDone($data, "thêm thành công",Response::HTTP_OK);
        } else{
            return $this->outPutError('Không tìm thấy dữ liệu!',Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function delete(){
        $request = request();
        $id = $request->id?:0;
        if(THIS::where('id',$id)->first()){
            if($request->id > 0) {
                $data = THIS::find($request->id);
                if ($data) {
                    THIS::destroy($id);
                    return $this->outputDone($data, "xóa thành công",Response::HTTP_OK);
                }
            }
        } else{
            return $this->outPutError('Không tìm thấy dữ liệu!',Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        if(!$id) {
            return $this->outPutError('Không tìm thấy dữ liệu!',Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        
    }
    public function show()
    {
        $id = request('id');
        $this_id = THIS::where('id',$id)->first();
        if($this_id){
            return $this->outputDone($this_id,true,Response::HTTP_OK);
        }else{
            return $this->outPutError('Không tìm thấy dữ liệu!',Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function update(){
        $id = request('id');
        if(request()->isMethod('post')){
            $validator = Validator::make( request()->all(), [
                'code'       => 'required|unique:coupons,code|max:10|min:3',
                'value'      => 'required|max:100',
                'status'     => 'required',
                'created'    =>  'required|date',
                'expired'    =>  'required|date|after_or_equal:created'
            ]);
            if($validator->fails()) {
                return $this->outPutError($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            $data = array(
                'code'=> request()->code,
                'status'=> request()->status,
                'created'=>strtotime(str_replace('/', '-',  request()->created)),
                'value'=> request()->value,
                'expired'=>strtotime(str_replace('/', '-',  request()->expired)),
            );
            if(THIS::where('id',$id)->update($data)){
                return $this->outputDone($data, "edit thành công ",Response::HTTP_OK);
            } else{
                return $this->outPutError('Không tìm thấy dữ liệu!',Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }
        return $this->outPutError('không tìm thấy method được sử dụng',Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
