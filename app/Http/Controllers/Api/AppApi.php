<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AppApi extends Controller
{
    private function _return($msg = '', $data = [], $status = 0) {
        $dt = [
            'status' => $status,
            'data' => $data,
            'msg' => $msg,
        ];

        header('Content-Type: application/json');
        header("HTTP/1.1 ".$status." OK");
        die(json_encode($dt));
    }

    public function outPutError($msg = 'error', $status = 0) {
        return $this->_return($msg, [], $status);
    }

    public function outPutDone($data = [], $msg = 'done', $status = 0) {
        die($this->_return($msg, $data, $status));
    }

    public function public_api($class = '', $method = ''){
        /**
         * $class : ten class format (name_api)
         * $method : ten function
         */
        $class .= '_api';
        // kiem tra url 
        if(!file_exists(__DIR__.'/'.$class.'.php')) {
            return $this->_return('Class ('.$class.') not found', [], -1);
        }
        
        require_once __DIR__.'/'.$class.'.php';
        $action = strtolower($method);
        $classObject =__NAMESPACE__.'\\'.$class;
        $classObject = new $classObject;
        $action = str_replace('-', '_', $action);
        if(!$action) {
            $action = 'index';
        }
        if(method_exists($classObject, $action)){
            return $classObject->$action();
        }

        $this->_return('Action ('.$class.'::'.$method .') not found', [], -1);
    }


}
