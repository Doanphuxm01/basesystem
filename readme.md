<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Khỏi tạo project 

<p>git clone </p>
<p>cd core</p>
<p>cấu hình admin và password trên file .env</p>
<p>chạy lệnh: </p>

- composer install
- php artisan migrate
- php artisan server
## Data-faker
    -http://127.0.0.1:8000/_job/coupon?coupon
## Danh sách Api
<p>1:danh sách  các mã Api </p>
    </br>
    - GET: http://127.0.0.1:8000/api/coupon/list
<p>2: xem 1 mã theo id </p>
    </br>
    -[GET,POST]: http://127.0.0.1:8000/api/coupon/show?id=8
<p>3: xóa mã coupon có id=20</p>
    -[GET,POST]: http://127.0.0.1:8000/api/coupon/delete?id=20
    </br>
<p>
4: thêm mã coupon theo param = [
    </br>
      'code',
      </br>
      'value',
      </br>
      'status',
      </br>
      'created',
      </br>
      'expired'
      </br>
    ]
</p>
    </br>
    - POST: http://127.0.0.1:8000/api/coupon/input
    </br>
<p>5: update 1 mã coupon theo id param = [ </br>
      'code',
      </br>
      'value',
      </br>
      'status',
      </br>
      'created',
      </br>
      'expired'
      </br>
 ]
 </p>
 </br>
- POST: http://127.0.0.1:8000/api/coupon/update
